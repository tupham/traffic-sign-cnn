import pickle, wget, zipfile
import wget
import numpy as np 
from sklearn.preprocessing import LabelBinarizer
from sklearn.metrics import mean_squared_error, accuracy_score
import sys, os
import json, yaml

import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import AveragePooling2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import concatenate
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.optimizers import SGD, RMSprop

tf.random.set_seed(
    0
)


def train():

    # directory to input file
    hyperparameter_input=sys.argv[1]

    # obtain dataset online
    if not os.path.isfile('traffic-signs-data.zip'):
        print("### Downloading data ###")
        url="https://d17h27t6h515a5.cloudfront.net/topher/2017/February/5898cd6f_traffic-signs-data/traffic-signs-data.zip"
        wget.download(url)
    if not os.path.isdir('data'):
        os.makedirs('data')
    
    # extract data files
    print("\n### Extracting data ###")
    with zipfile.ZipFile("traffic-signs-data.zip", "r") as f:
        f.extractall("data")

    # read data
    with open('data/train.p', mode="rb") as f:
        train=pickle.load(f)
        trainX, trainY = train["features"], train["labels"]
        print(f"TrainX dimensions: {trainX.shape}")
        trainX = trainX.astype("float") / 255.0
    with open('data/valid.p', mode="rb") as f:
        valid=pickle.load(f)
        validX, validY = valid["features"], valid["labels"]
        print(f"ValidX dimensions: {validX.shape}")
        validX = validX.astype("float") / 255.0
    with open('data/test.p', mode="rb") as f:
        test=pickle.load(f)
        testX, testY = test["features"], test["labels"]
        print(f"TestX dimensions: {testX.shape}")
        testX = testX.astype("float") / 255.0
    print('### Datasets Loaded ###')

    # data preprocessing, convert categorical output to one-hot vector
    lb = LabelBinarizer()
    trainY = lb.fit_transform(trainY)
    validY = lb.fit_transform(validY)

    # build CNN model
    print("### Building the neural network ###")
    model = Sequential()
    width = 32
    height = 32
    classes = 43
    shape = (width, height, 3)

    initializer=tf.keras.initializers.GlorotUniform(
        seed=0
    )


    model.add(Conv2D(32, (3, 3), padding="same", input_shape=shape, kernel_initializer=initializer))
    model.add(Activation("relu"))
    model.add(BatchNormalization())
    model.add(Conv2D(32, (3,3), padding="same", kernel_initializer=initializer))
    model.add(Activation("relu"))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(64, (3,3), padding="same", kernel_initializer=initializer))
    model.add(Activation("relu"))
    model.add(BatchNormalization())
    model.add(Conv2D(64, (3,3), padding="same", kernel_initializer=initializer))
    model.add(Activation("relu"))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Flatten())
    model.add(Dense(512))
    model.add(Activation("relu"))
    model.add(BatchNormalization())
    model.add(Dense(classes))
    model.add(Activation("softmax"))

    print("Model summary: ")
    print(model.summary)

    # define image pseudo-data generator to generate shifted/rotated images for training
    aug=ImageDataGenerator(rotation_range=0.18, zoom_range=0.15, width_shift_range=.2, height_shift_range=0.2, horizontal_flip=True)

    # load hyperparameters from input.json
    print("### Loading hyperaparameter input ###")
    with open(hyperparameter_input, 'r') as f:
        params = json.load(f)
        print(f"Input hyperparameters: \n\n{yaml.dump(params)}")
    
    params['learning_rate'] = float(params['learning_rate'])
    params['epochs'] = int(np.ceil(params['epochs']))
    params['batch_size'] = int(2 ** (np.floor(params['log2_batch_size'])))
    params['momentum'] = 1 - 10**( - (params['log10_momentum']))
    
    
    # define optimizer using configurations defined in inputs
    opt=RMSprop(learning_rate=params["learning_rate"], momentum=params["momentum"])

    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=["accuracy"])

    # train model
    print("### Begin Training ###")
    H=model.fit(aug.flow(trainX, trainY, batch_size=params["batch_size"]), validation_data=(validX, validY), steps_per_epoch=trainX.shape[0]//params["batch_size"], epochs=params["epochs"], verbose=1)

    # predict
    pred=model.predict(testX)
    final=np.argmax(pred, axis=1)

    # calculate scores
    score = mean_squared_error(y_true=test["labels"], y_pred=final)
    accuracy = accuracy_score(test["labels"], final)
    print(f'Test root mean squared error: {np.sqrt(score)}')
    print(f'Accuracy: {accuracy}')

    # (IMPORTANT) scores to output.json
    with open("output.json", "w") as f:
        json.dump({'loss': score, 'accuracy': accuracy}, f)
    
    params['loss'] = score
    params['accuracy'] = accuracy

    if os.path.isfile("evaluation_output.json"):
        with open("evaluation_output.json", 'r') as f:
            outputs = json.load(f)
    else:
        outputs = []
    
    outputs.append(params)

    with open("evaluation_output.json",'w') as f:
        json.dump(outputs, f)
    
    return 

if __name__ == '__main__':
    train()
