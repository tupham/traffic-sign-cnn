FROM tensorflow/tensorflow:latest

ARG PROJECT=ATLASMLHPO

RUN mkdir ${PROJECT}

RUN apt-get update 
RUN apt-get install -y --no-install-recommends git wget unzip bzip2 build-essential ca-certificates graphviz vim
RUN apt-get clean

RUN pip install --upgrade pip

COPY ./train_script ${PROJECT}/train_script
COPY ./example_config ${PROJECT}/example_config
COPY ./requirements.txt ${PROJECT}/requirements.txt

RUN pip install -r ${PROJECT}/requirements.txt